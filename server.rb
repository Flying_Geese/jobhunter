require 'sinatra'
require 'nokogiri'
require 'open-uri'

get '/' do
  erb :home
end

get '/results' do
  @res = params[:query]
  @doc = Nokogiri::HTML(open("https://www.indeed.com/jobs?q=#{@res}&l=new+york%2C+ny"))
  @jobs = @doc.search('.jobtitle')
  generate_cl(@jobs)
  erb :results
end

def generate_cl(jobs)
  jobs.each { |job|
    File.open("/home/mlaw/Projects/jobhunter/letters/#{job.text}-letter.txt", "w") { |letter|
      letter.write("Dear Fam, \n
        About that young #{job} position, I was thinking, hey ... why not\n
        give your boy a chance?\n
        Sincerely,\n
        Your Boi
        ")
      }
    }
end
